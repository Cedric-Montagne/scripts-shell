#!/bin/bash

# Bash script for activating or inactivating a laptop touchpad.
# Needs to be associated to a keyboard shortcut or launched at session
# startup in order to invert the touchpad state.
#
# Copyleft 2015, Cédric Montagne
# https://framasphere.org/u/cedric_montagne
# License : GPLv3
# License text can be accessed here :
# http://www.gnu.org/licenses/gpl.html

# To adapt it to your configuration, you first need to use the
# "xinput list" command in order to find your touchpad name as it is
# identified by the system.
# Then you'll need to insert this name between quotation marks, in
# place of the "PS/2 Logitech Wheel Mouse" mentioned in the line
# beginning with id bellow.

id=$(xinput list | grep "PS/2 Logitech Wheel Mouse" | cut -d = -f 2 | cut -c 1,2)

# With the previous command we just retrieved the touchpad
# identification number so that it is recorded in the id variable.
# Indeed this number can change, depending on USB devices plugged
# while the present script is being executed.

# Let's now use the id variable to get the touchpad state and record
# it into the state variable.

state=$(xinput --list-props $id | grep "Device Enabled" | cut -d : -f 2)

# If state variable equal 1 it means the touchpad is activated. 
# Then let's inactivate the touchpad.

if [ $state -eq 1 ]
then
  xinput set-prop $id "Device Enabled" 0

# Else, if state variable equal 0, it means the touchpad is
# inactivated.
# Then let's activate the touchpad.

elif [ $state -eq 0 ]
then
  xinput set-prop $id "Device Enabled" 1
  
# End of test

fi

# End of script with zero end state (success)

exit 0
