#!/bin/bash

# Script bash pour activer ou désactiver le pavé tactile d'un
# ordinateur portable.
# Nécessite d'être associé à un raccourci clavier ou lancé au
# démarrage d'une session afin d'inverser l'état du pavé tactile.
#
# Copyleft 2015, Cédric Montagne
# https://framasphere.org/u/cedric_montagne
# Licence : GPLv3
# Le texte de la licence peut être consulté à l'adresse suivante :
# http://www.gnu.org/licenses/gpl.html

# Pour adapter ce script à votre configuration, vous devez
# préalablement utiliser la commande "xinput list" afin de trouver le
# nom de votre pavé tactile tel qu'il est reconnu par le système.
# Vous devrez alors insérer ce nom entre guillemets, à la place de la
# mention "PS/2 Logitech Wheel Mouse" qui figure dans la ligne
# commençant par id ci-dessous.

id=$(xinput list | grep "PS/2 Logitech Wheel Mouse" | cut -d = -f 2 | cut -c 1,2)

# Avec la précédente commande nous venons de récupérer le numéro
# d'identifiant du pavé tactile pour le stocker dans la variable id.
# En effet, ce numéro peut changer selon les périphériques USB qui
# sont branchés au moment de l'exécution du présent script.

# Utilisons maintenant la variable id pour obtenir l'état du pavé
# tactile et le stocker dans la variable etat.

etat=$(xinput --list-props $id | grep "Device Enabled" | cut -d : -f 2)

# Si la variable etat est égale à 1 c'est que le pavé tactile est
# activé.
# Désactivons alors le pavé tactile.

if [ $etat -eq 1 ]
then
  xinput set-prop $id "Device Enabled" 0

# Sinon, si la variable etat est égale à 0, c'est que le pavé tactile
# est désactivé.
# Activons alors le pavé tactile.

elif [ $etat -eq 0 ]
then
  xinput set-prop $id "Device Enabled" 1
  
# Fin du test

fi

# Fin du script avec état de sortie nul (réussite)

exit 0
